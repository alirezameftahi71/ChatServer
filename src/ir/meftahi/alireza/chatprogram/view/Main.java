package ir.meftahi.alireza.chatprogram.view;

import ir.meftahi.alireza.chatprogram.model.Server;

public class Main {

	public static void main(String[] args) {
		try {
			Thread serverThread = new Thread(new Server(10001));
			serverThread.start();
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

}
