package ir.meftahi.alireza.chatprogram.control;

import java.net.Socket;

public class Util {
	public static String getIp(String message) {
		return message.split("@")[0];
	}

	public static String getMessage(String message) {
		return message.split("@")[1];
	}
	
	public static Socket findSocketByIp(String ip) {
		if (ip == null)
			throw new NullPointerException();
		for (Socket socket : Db.getSockets()) {
			if (socket.getInetAddress().toString().replace("/", "").equals(ip)) {
				return socket;
			}
		}
		return null;
	}

}
