package ir.meftahi.alireza.chatprogram.control;

import java.net.Socket;
import java.util.Vector;

public class Db {
	private static Vector<Socket> sockets = new Vector<>();

	public static Vector<Socket> getSockets() {
		return sockets;
	}

	public static void addSocket(Socket socket) {
		Db.sockets.add(socket);
	}

	public static void deleteSocket(Socket socket) {
		Db.sockets.remove(socket);
	}
}
