package ir.meftahi.alireza.chatprogram.model;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import ir.meftahi.alireza.chatprogram.control.Db;

public class Server implements Runnable {
	private ServerSocket serverSocket = null;

	public Server(Integer port) throws Exception {
		if (port == null)
			throw new Exception("Port is null");
		serverSocket = new ServerSocket(port);
	}

	@Override
	public void run() {
		while (true) {
			Socket socket;
			try {
				socket = serverSocket.accept();
				System.out.println(socket.getInetAddress().toString() + " has connected.");
				Db.addSocket(socket);
				Thread chatChannelThread = new Thread(new ChatChannel(socket));
				chatChannelThread.start();
			} catch (IOException e) {
				System.err.println(e.getMessage());
				break;
			}
		}
	}

}
