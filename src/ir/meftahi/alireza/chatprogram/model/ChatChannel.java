package ir.meftahi.alireza.chatprogram.model;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Formatter;
import java.util.NoSuchElementException;
import java.util.Scanner;

import ir.meftahi.alireza.chatprogram.control.Util;

// Sends message
public class ChatChannel implements Runnable {
	private Scanner getter;
	private Formatter formatter = null;
	private Socket socket = null;

	public ChatChannel(Socket socket) throws IOException {
		this.socket = socket;
		this.getter = new Scanner(this.socket.getInputStream());
	}

	@Override
	public void run() {
		while (true) {
			try {
				String message = getter.nextLine();
				Socket targetSocket = Util.findSocketByIp(Util.getIp(message));
				OutputStream outputStream = targetSocket.getOutputStream();
				formatter = new Formatter(outputStream);
				formatter.format("%s%n", Util.getMessage(message)).flush();
			} catch (NullPointerException e) {
				System.err.println("Target not found.");
				continue;
			} catch (IOException e) {
				System.err.println(e.getMessage());
				continue;
			} catch (ArrayIndexOutOfBoundsException e) {
				System.err.println("Bad formatted message.");
				continue;
			} catch (NoSuchElementException e) {
				System.out.println(this.socket.getInetAddress().toString() + " has disconnected.");
				break;
			}
		}
	}
}
