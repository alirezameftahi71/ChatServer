# ChatProgramServer
A simple console based chat program server. Where clients can connect to and chat to each other using their target client's IP.<br />
To use this app, run it, then use telnet to connect to it using port '10001' then you can test it by messaging yourself in the following format:<br />
[target ip]@[your message]<br />
ex : 192.168.1.2@Hello World!<br />
